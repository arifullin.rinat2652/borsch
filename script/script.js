window.addEventListener('DOMContentLoaded', () => {
  const burger = document.getElementById('burger');
  const menu = document.getElementById('menu');
  const openButton = document.querySelectorAll('.js-open-modal');
  const modal = document.getElementById('modal');
  const tabsBtn = document.querySelectorAll('.img-plus');
  const tabsItems =  document.querySelectorAll('.tabs__item');
  const openBtnCard = document.querySelectorAll('.card__btn')
  const cardItems = document.querySelectorAll('.card')


  burger.addEventListener('click', () => {
    menu.classList.toggle('is-active')
  });



  openButton.forEach((element) => {
    element.addEventListener('click', () => {
      modal.style.display = 'block';
   })
  });



  modal.querySelector('.modal-dialog').addEventListener('click', event => {
    event._isClickWithinModal = 'true'
  })

  modal.addEventListener('click', event => {
    if (event._isClickWithinModal) return;
    modal.style.display = 'none';
  })



  openBtnCard.forEach(function(item) {
    item.addEventListener('click', () => {
      let currentBtn = item;
      let btnId = currentBtn.getAttribute('data-btn');
      let currentCard = document.querySelector(btnId);


      if (!currentBtn.classList.contains('active')) {

      openBtnCard.forEach((item) => {
        item.classList.remove('active');
      })

      cardItems.forEach((item) => {
        item.classList.remove('active')
      })

      currentBtn.classList.add('active');
      currentCard.classList.add('active');
      };
    });
  });






  tabsBtn.forEach(function(item) {
    item.addEventListener('click', () => {
      let currentBtn = item;
      let tabId = currentBtn.getAttribute('data-tab');
      let currentTab = document.querySelector(tabId);


      if (!currentBtn.classList.contains('active')) {

      tabsBtn.forEach((item) => {
        item.classList.remove('active');
      })

      tabsItems.forEach((item) => {
        item.classList.remove('active')
      })

      currentBtn.classList.add('active');
      currentTab.classList.add('active');
      };
    });
  });

  const nextSlide = document.getElementById('next')
  const previousSlide = document.getElementById('previous')

  let slideIndex = 1;
  showSlides(slideIndex);

  nextSlide.addEventListener('click', () => {
    showSlides(slideIndex += 1);
  })



  previousSlide.addEventListener('click', () => {
    showSlides(slideIndex -= 1);
  })



  function currentSlide(n) {
    showSlides(slideIndex = n);
  }


  function showSlides(n) {
    let slides = document.querySelectorAll('.item');

    if (n > slides.length) {
      slideIndex = 1
    }
    if (n < 1) {
      slideIndex = slides.length
    }

    for (let slide of slides) {
      slide.style.display = "none";
    }

    slides[slideIndex - 1].style.display = "block";
  };

    let autoSlide = setInterval(() => showSlides(slideIndex += 1), 3000);


    nextSlide.addEventListener('click', (event) => {
      event._isClickOnBtnNext = 'true'
    })

    previousSlide.addEventListener('click', (event) => {
      event._isClickOnBtnPrevious = 'true'
    })

    nextSlide.addEventListener('click', (event) => {
      if (event._isClickOnBtnNext){
        clearInterval(autoSlide)
      }
    })

    previousSlide.addEventListener('click', (event) => {
      if (event._isClickOnBtnPrevious){
        clearInterval(autoSlide)
      }
    })


  document.querySelector('.img-plus').click();
  document.querySelector('.card__btn').click();


});
